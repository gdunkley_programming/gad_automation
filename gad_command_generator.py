from GADcommands import PySwitch, GADprocess

def main():

    # variables for gadcmd
    username = None
    password = None
    ldev_id = None
    quorum = None
    hostgrp = None
    host_mode = None
    host_mode_opt = None
    horcm_ldev_grp = None
    horcm_id = None
    resource_grp_name = None
    type_storage = None
    serial_storage = None
    serial_vsm = None
    port_id_fabA = None
    port_id_fabB = None

    def fabric(fab):
        if fab is True:
            port_id = port_id_fabA
        else:
            port_id = port_id_fabB

        return port_id

    port_id = fabric(True)

    config_commands = []

    pyswitch = PySwitch()

    loop_menu_main = True

    while loop_menu_main:
        pyswitch.switch(1)
        obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt, horcm_id,
                                    resource_grp_name, type_storage, serial_storage, serial_vsm, port_id, horcm_ldev_grp)
        choice_main = int(input("Enter your choice [1-4]: "))

        if choice_main == 1:
            loop_menu1 = True

            while loop_menu1:
                pyswitch.switch(2)
                choice_menu1 = int(input("Enter your choice [1-7]: "))

                if choice_menu1 == 1:

                    print('\n Enter login information')
                    username = input("username : ")
                    password = input("password : ")

                elif choice_menu1 == 2:

                    print("\n Please enter the following values:")
                    horcm_id = input("horcm id number : ")
                    horcm_ldev_grp = input("horcm Ldev Group Name : ")
                    serial_storage = input("storage serial number : ")
                    type_storage = input("storage model type (ie R800) : ")
                    resource_grp_name = input("VSM resource group name : ")
                    serial_vsm = input("primary storage VSM number (ie primary / virtual serial : ")
                    quorum = input("quorum number : ")
                    port_id_fabA = input("port number FabA <CL7-L-3> : ")
                    port_id_fabB = input("port number FabB <CL8-L-3> : ")
                    hostgrp = input("host group name : ")
                    host_mode = input("host group mode : ")
                    host_mode_opt = input('host mode options : ')
                    ldev_id = input("Ldev id <0x0701> : ")


                elif choice_menu1 == 3:

                    print("\n Please enter the following values:")
                    port_id_fabA = input("port number FabA <CL7-L-3> : ")
                    port_id_fabB = input("port number FabB <CL7-L-3> : ")

                elif choice_menu1 == 4:

                    print("\n Please enter the following values:")
                    ldev_id = input("Ldev id : ")

                elif choice_menu1 == 5:

                    print("\n Please enter the following values:")
                    serial_storage = input("storage serial number : ")

                elif choice_menu1 == 6:

                    print('\n Current values are set as:')
                    print("username : " + username)
                    print("password : " + password)
                    print("horcm id number : " + str(horcm_id))
                    print("horcm Ldev Group name : " + str(horcm_ldev_grp))
                    print("storage serial number : " + str(serial_storage))
                    print("storage model type (ie R800) : " + type_storage)
                    print("VSM resource group name : " + resource_grp_name)
                    print("storage VSM number (ie primary / virtual serial : " + str(serial_vsm))
                    print("quorum number : " + str(quorum))
                    print("Fabric A - port number : " + fabric(True))
                    print("Fabric B - port number : " + fabric(False))
                    print("host group name : " + hostgrp)
                    print("host group mode : " + host_mode)
                    print('host group mode options : ' + host_mode_opt)
                    print("Ldev id : " + str(ldev_id))

                elif choice_menu1 == 7:
                    loop_menu1 = False

                else:
                    print("\n Incorrect selection Please select a valid option")

        elif choice_main is 2:
            loop_menu2 = True

            while loop_menu2:
                pyswitch.switch(3)
                obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt,
                                                horcm_id, resource_grp_name, type_storage, serial_storage, serial_vsm,
                                                port_id, horcm_ldev_grp)
                choice_menu2 = int(input("Enter your choice [1-9]: "))

                if choice_menu2 is 1:
                    print('\nLogin and logout information :\n')
                    config_commands.append(obj_gadprocess.login())
                    config_commands.append(obj_gadprocess.logoff())

                elif choice_menu2 is 2:
                    print('\nCheck volume :\n')
                    config_commands.append(obj_gadprocess.check_volume())

                elif choice_menu2 is 3:
                    print('\nVirtualization P-Vol :\n')
                    config_commands.append(obj_gadprocess.pvol_virtulization())

                elif choice_menu2 is 4:
                    print('\nVirtualization S-Vol :\n')
                    config_commands.append(obj_gadprocess.check_volume())
                    config_commands.append(obj_gadprocess.alura_prefered_path_enable())
                    config_commands.append(obj_gadprocess.svol_virtulization())


                elif choice_menu2 is 5:
                    print('\nHost group reservation (new) :\n')
                    port_id = fabric(False)
                    obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt,
                                                horcm_id, resource_grp_name, type_storage, serial_storage, serial_vsm,
                                                port_id, horcm_ldev_grp)
                    config_commands.append(obj_gadprocess.host_group_reservation_new())

                    port_id = fabric(True)
                    obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt,
                                                horcm_id, resource_grp_name, type_storage, serial_storage, serial_vsm,
                                                port_id, horcm_ldev_grp)
                    config_commands.append(obj_gadprocess.host_group_reservation_new())

                elif choice_menu2 is 6:
                    print('\nEnabling Alua on volume:\n')
                    config_commands.append(obj_gadprocess.alura_prefered_path_enable())

                elif choice_menu2 is 7:
                    print("\n Please enter the following values:")
                    ldev_id = input("Ldev id : ")


                elif choice_menu2 is 8:
                    print('\nStandard GAD build S-vols (1, 2, 4-7) :\n')
                    config_commands.append(obj_gadprocess.login())
                    config_commands.append(obj_gadprocess.check_volume())
                    config_commands.append(obj_gadprocess.svol_virtulization())
                    port_id = fabric(False)
                    obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt,
                                                horcm_id, resource_grp_name, type_storage, serial_storage, serial_vsm,
                                                port_id, horcm_ldev_grp)
                    config_commands.append(obj_gadprocess.host_group_reservation_new())

                    port_id = fabric(True)
                    obj_gadprocess = GADprocess(username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt,
                                                horcm_id, resource_grp_name, type_storage, serial_storage, serial_vsm,
                                                port_id, horcm_ldev_grp)
                    config_commands.append(obj_gadprocess.host_group_reservation_new())
                    config_commands.append(obj_gadprocess.alura_prefered_path_enable())
                    config_commands.append(obj_gadprocess.logoff())

                elif choice_menu2 is 9:
                    loop_menu2 = False

                else:
                    print("Incorrect selection Please select a valid option")

        elif choice_main is 3:
            loop_menu3 = True

            while loop_menu3:
                pyswitch.switch(4)
                choice_menu3 = int(input("Enter your choice [1-7]: "))

                if choice_menu3 is 1:
                    print('\nDisplay GAD pairs :\n')
                    config_commands.append(obj_gadprocess.gad_display_pair())

                elif choice_menu3 is 2:
                    print('\nCreating GAD pairs :\n')
                    config_commands.append(obj_gadprocess.gad_create_pair())

                elif choice_menu3 is 3:
                    print('\nSuspending GAD pairs :\n')
                    config_commands.append(obj_gadprocess.gad_suspend_pair())

                elif choice_menu3 is 4:
                    print('\nResynchronizing GAD pairs :\n')
                    config_commands.append(obj_gadprocess.gad_resync_pair())

                elif choice_menu3 is 5:
                    print('\nDeleting GAD pairs :\n')
                    config_commands.append(obj_gadprocess.gad_delete_pair())

                elif choice_menu3 is 6:
                    print('\nSwapping Pvol-Svol @ Secondary site(svol) :\n')
                    config_commands.append(obj_gadprocess.gad_swapPvolSvol())

                elif choice_menu3 is 7:
                    loop_menu3 = False

                else:
                    print("Incorrect selection Please select a valid option")

        elif choice_main is 4:
            print("Exiting... and saving file\n")
            print('Total Process: ')
            for i in config_commands:
                print(i)
            output_to_txt(config_commands)
            loop_menu_main = False

        else:
            print("Incorrect selection Please select a valid option")




def output_to_txt(mylist):
    with open('config_commands.txt', 'a') as outfile:
        for var in mylist:
            outfile.write('%s\n' % var)

    outfile.close()


main()

