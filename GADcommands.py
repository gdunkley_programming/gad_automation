class GADcmd(object):

    def __init__(self, username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt, horcm_id, resource_grp_name,
                 type_storage, serial_storage, serial_vsm, port_id, horcm_ldev_grp):

        # creation variables
        self.usr = str(username)
        self.pwd = str(password)
        self.quorum = str(quorum)
        self.hostgrp = str(hostgrp)
        self.host_mode = str(host_mode)
        self.host_mode_opt = str(host_mode_opt)
        self.horcm_id = str(horcm_id)
        self.resource_grp_name = str(resource_grp_name)
        self.serial_vsm = str(serial_vsm)
        self.serial_storage = str(serial_storage)
        self.type_storage = str(type_storage)
        self.port_id = str(port_id)
        self.ldev_id = str(ldev_id)
        self.horcm_ldev_grp = str(horcm_ldev_grp)



    # Adding
    def add_hostgrp_port(self):
        cmd = 'raidcom add host_grp -port ' + self.port_id + ' -host_grp_name ' + self.hostgrp + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def add_ldev(self):
        cmd = 'raidcom add lun -port ' + self.port_id + ' -ldev_id ' + self.ldev_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def add_resource_group(self):
        cmd = 'raidcom add resource -resource_name ' + self.resource_grp_name + ' -virtual_type ' + self.type_storage + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def add_resouce_ldev(self):
        cmd = 'raidcom add resource -resource_name ' + self.resource_grp_name + ' -ldev_id ' + self.ldev_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def add_resource_port(self):
        cmd = 'raidcom add resource -resource_name ' + self.resource_grp_name + ' -port ' + self.port_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    # Getting
    def get_hostgrp(self):
        cmd = 'raidcom get host_grp -port ' + self.port_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def get_hostgrp_ldevs(self):
        cmd = 'raidcom get lun -port ' + self.port_id + ' -fx -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def get_resource_group(self):
        cmd = 'raidcom get resource -key opt -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def get_ldev(self):
        cmd = 'raidcom get ldev -ldev_id ' + self.ldev_id + ' -fx' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def get_ldev_frontend(self):
        cmd = 'raidcom get ldev -ldev_id ' + self.ldev_id + ' -key front_end -cnt 1 -fx' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    # Access
    def login_horcm(self):
        cmd = 'raidcom -login ' + self.username + ' ' + self.password + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def logout_horcm(self):
        cmd = 'raidcom -logout' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    # Mapping
    def map_resouce_ldev_reserved(self):
        cmd = 'raidcom map resource -ldev_id ' + self.ldev_id + ' -virtual_ldev_id reserve' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def map_resouce_ldev_virtual(self):
        cmd = 'raidcom map resource -ldev_id ' + self.ldev_id + ' -virtual_ldev_id ' + self.ldev_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def mod_hostgrp_mode(self):
        cmd = 'raidcom modify host_grp -port ' + self.port_id + ' -host_mode ' + self.host_mode + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def unmap_resource_ldev(self):
        cmd = 'raidcom unmap resource -ldev_id ' + self.ldev_id + ' -virtual_ldev_id ' + self.ldev_id + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def horcm_start(self):
        cmd = 'horcmstart ' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def horcm_stop(self):
        cmd = 'horcmshutdown ' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def horcm_login(self):
        cmd = 'raidcom -login ' + self.usr + ' ' + self.pwd + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def horcm_logoff(self):
        cmd = 'raidcom -logout -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    # Modify
    def modify_hostgrp(self):
        cmd = 'raidcom modify host_grp -port ' + self.port_id + ' -host_mode ' + self.host_mode + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def modify_hostgrp_opt(self):
        cmd = 'raidcom modify host_grp -port ' + self.port_id + ' -host_mode ' + self.host_mode + ' -host_mode_opt ' + self.host_mode_opt + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def modify_ldev_ALUA_enabled(self):
        cmd = 'raidcom modify ldev -ldev_id ' + self.ldev_id + ' -alua enable ' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def modify_port_optimized(self):
        cmd = 'raidcom modify lun -port ' + self.port_id + '-lun_id all -asymmetric_access_state optimized' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def modify_port_non_optimized(self):
        cmd = 'raidcom modify lun -port ' + self.port_id + '-lun_id all -asymmetric_access_state non_optimized' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    # GAD Pair commands
    def paircreate(self):
        cmd = 'paircreate -g ' + self.horcm_ldev_grp + ' -fg never -vl -jq ' + self.quorum + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairdisplay(self):
        cmd = 'pairdisplay -g ' + self.horcm_ldev_grp + ' -fxce -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairresync(self):
        cmd = 'pairresync -g ' + self.horcm_ldev_grp + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairresync_reverse(self):
        cmd = 'pairresync -g ' + self.horcm_ldev_grp + '-swaps' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairsplit(self):
        cmd = 'pairsplit -g ' + self.horcm_ldev_grp + ' -r -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairsplit_delete(self):
        cmd = 'pairsplit -g ' + self.horcm_ldev_grp + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd

    def pairsplit_reverse(self):
        cmd = 'pairsplit -g ' + self.horcm_ldev_grp + ' -RS ' + ' -IH' + self.horcm_id + ' -s ' + self.serial_storage
        return cmd




class GADprocess(GADcmd):

    def __init__(self, username, password, ldev_id, quorum, hostgrp, host_mode, host_mode_opt, horcm_id, resource_grp_name,
                 type_storage, serial_storage, serial_vsm, port_id, horcm_ldev_grp):
        super()
        self.usr = str(username)
        self.pwd = str(password)
        self.quorum = str(quorum)
        self.hostgrp = str(hostgrp)
        self.host_mode = str(host_mode)
        self.host_mode_opt = str(host_mode_opt)
        self.horcm_id = str(horcm_id)
        self.resource_grp_name = str(resource_grp_name)
        self.serial_vsm = str(serial_vsm)
        self.serial_storage = str(serial_storage)
        self.type_storage = str(type_storage)
        self.port_id = str(port_id)
        self.ldev_id = str(ldev_id)
        self.horcm_ldev_grp = str(horcm_ldev_grp)

    def login(self):
        print(super(GADprocess, self).horcm_start()
              + '\n'
              + super(GADprocess, self).horcm_login())
        out = super(GADprocess, self).horcm_start() + '\n' + super(GADprocess, self).horcm_login()
        return out

    def logoff(self):
        print(super(GADprocess, self).horcm_logoff())
        out = super(GADprocess, self).horcm_logoff()
        return out


    def check_volume(self):
        print(super(GADprocess, self).get_ldev())
        out = super(GADprocess, self).get_ldev()
        return out

    def pvol_virtulization(self):
        print(super(GADprocess, self).unmap_resource_ldev())
        print(super(GADprocess, self).add_resouce_ldev())
        print(super(GADprocess, self).map_resouce_ldev_virtual())
        print(super(GADprocess, self).get_ldev())

        out = super(GADprocess, self).unmap_resource_ldev() + '\n'\
              + super(GADprocess, self).add_resouce_ldev() + '\n'\
              + super(GADprocess, self).map_resouce_ldev_virtual() + '\n'\
              + super(GADprocess, self).get_ldev()
        return out


    def svol_virtulization(self):
        print(super(GADprocess, self).unmap_resource_ldev())
        print(super(GADprocess, self).add_resouce_ldev())
        print(super(GADprocess, self).map_resouce_ldev_reserved())
        print(super(GADprocess, self).get_ldev())

        out = super(GADprocess, self).unmap_resource_ldev() + '\n'\
              + super(GADprocess, self).add_resouce_ldev() + '\n'\
              + super(GADprocess, self).map_resouce_ldev_reserved() + '\n'\
              + super(GADprocess, self).get_ldev()
        return out

    def host_group_reservation_new(self):
        print(super(GADprocess, self).add_hostgrp_port())
        print(super(GADprocess, self).modify_hostgrp_opt())
        print(super(GADprocess, self).add_resource_port())
        print(super(GADprocess, self).get_hostgrp())
        out = super(GADprocess, self).add_hostgrp_port() + '\n'\
              + super(GADprocess, self).modify_hostgrp_opt() + '\n'\
              + super(GADprocess, self).add_resource_port() + '\n' \
              + super(GADprocess, self).get_hostgrp()
        return out

    def alura_prefered_path_enable(self):
        print(super(GADprocess, self).modify_ldev_ALUA_enabled())
        out = super(GADprocess, self).modify_ldev_ALUA_enabled()
        return out

    def gad_display_pair(self):
        print(super(GADprocess, self).pairdisplay())
        out = super(GADprocess, self).pairdisplay()
        return out

    def gad_create_pair(self):
        print(super(GADprocess, self).paircreate())
        out = super(GADprocess, self).paircreate()
        return out

    def gad_suspend_pair(self):
        print(super(GADprocess, self).pairsplit())
        out = super(GADprocess, self).pairsplit()
        return out

    def gad_resync_pair(self):
        print(super(GADprocess, self).pairresync())
        out = super(GADprocess, self).pairresync()
        return out

    def gad_delete_pair(self):
        print(super(GADprocess, self).pairsplit() + '\n'
              + super(GADprocess, self).pairsplit_delete())
        out = super(GADprocess, self).pairsplit() + '\n'\
              + super(GADprocess, self).pairsplit_delete()
        return out

    def gad_swapPvolSvol(self):
        print(super(GADprocess, self).pairsplit_reverse() + '\n' + super(GADprocess, self).pairresync_reverse())
        out = super(GADprocess, self).pairsplit_reverse() + '\n' + super(GADprocess, self).pairresync_reverse()
        return out


class PySwitch:

    def switch(self, selection):
        default = "Incorrect Option"
        return getattr(self, 'case_' + str(selection), lambda: default)()

    def case_1(self):
        # Main Menu
        print("\n")
        print(30 * "-" , "MENU" , 30 * "-")
        print("1. Enter GAD Configuration variables")
        print("2. Get GAD Configuration Commands")
        print("3. Get GAD Replication Commands")
        print("4. Save config file and Exit")
        print(67 * "-")
        return '\n'

    def case_2(self):
        # menu 1
        print("\n")
        print(21 * "-" , "Enter/Change Variables" , 21 * "-")
        print("1. Enter login information")
        print("2. Enter Variables for GAD setup")
        print("3. Change Ports")
        print("4. Change Ldev id")
        print("5. Change Storage Serial")
        print("6. Show Current Variables")
        print("7. Back")
        print(67 * "-")
        return '\n'

    def case_3(self):
        # menu 2
        print("\n")
        print(19 * "-", "GAD Configuration Commands", 19 * "-")
        print("1. logging In and logout")
        print("2. Check Volume")
        print("3. P-vol virtualization (primary VSM)")
        print("4. S-vol virtualization (secondary VSM, including Alua mode)")
        print("5. New host group reservation in VSM")
        print("6. Enabling Alua on Volume")
        print("7. Change Ldev id")
        print("8. Standard GAD build S-vols (1, 2, 4-7)")
        print("9. Back")
        print(67 * "-")
        return '\n'

    def case_4(self):
        # menu 3
        print("\n")
        print(20 * "-", "GAD Replication Commands", 20 * "-")
        print("1. Display GAD pairs")
        print("2. Creating GAD pairs")
        print("3. Suspending GAD pairs")
        print("4. Resynchronizing GAD pairs")
        print("5. Deleting GAD pairs")
        print("6. Swapping Pvol-Svol @ Secondary site(svol)")
        print("7. Back")
        print(67 * "-")
        return '\n'

